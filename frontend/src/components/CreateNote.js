import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default class CreateNote extends Component {

  state = {
    title: '',
    content: '',
    date: new Date(),
    userSelected: '',
    users: [],
    editing: false,
    _id: ''
  }

  async componentDidMount(){
    let res = await axios.get('http://localhost:4000/api/users');
    this.setState({users: res.data});
    this.setState({ userSelected: res.data[0].username });

    if (this.props.match.params.id) {
      console.log(this.props.match.params.id)
      let res = await axios.get('http://localhost:4000/api/notes/' + this.props.match.params.id);
      console.log(res.data)
      this.setState({
          title: res.data.title,
          content: res.data.content,
          date: new Date(res.data.date),
          userSelected: res.data.author,
          _id: res.data._id,
          editing: true
      });
    }
  }

  onSubmit = async (e) => {
    e.preventDefault();
    let newNote = {
      title: this.state.title,
      content: this.state.content,
      author: this.state.userSelected,
      date: this.state.date
    };
    if (this.state.editing) {
      await axios.put('http://localhost:4000/api/notes/' + this.state._id, newNote);
    } else {
      axios.post('http://localhost:4000/api/notes', newNote);
    }
    window.location.href = '/';
  }

  onInputChange = (e) => {
    this.setState({
        [e.target.name]: e.target.value
    })
  }

  onChangeDate = date => {
    this.setState({ date });
  }

  render() {
    return (
      <div className="col-md-6 offset-md-3">
        <div className="card">

          <div className="card-body">
            <h4>Create a note</h4>
            <form onSubmit={this.onSubmit}>

              <div className="form-group">
                <select name="userSelected" className="form-control" value={this.state.userSelected} onChange={this.onInputChange}>
                  {
                    this.state.users.map(user => (
                      <option value={user.username} key={user._id}>
                        {user.username}
                      </option>)
                    )
                  }
                </select>
              </div>

              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Title"
                  onChange={this.onInputChange}
                  name="title"
                  value={this.state.title}
                  required />
              </div>

              <div className="form-group">
                <textarea
                  type="text"
                  className="form-control"
                  placeholder="Content"
                  name="content"
                  onChange={this.onInputChange}
                  value={this.state.content}
                  required>
                </textarea>
              </div>

              <div className="form-group">
                <DatePicker className="form-control" selected={this.state.date} onChange={this.onChangeDate} />
              </div>

              <button type="submit" className="btn btn-primary">Save</button>
            </form>
          </div>

        </div>
      </div>
    )
  }
}
